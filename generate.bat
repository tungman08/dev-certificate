@echo off

set output_directory=output
if exist %output_directory% (
    rmdir /s /q %output_directory%
)

mkdir %output_directory%

REM Generate root certificate files.
echo Generate root certificat files.
set root_cert=root-cert
openssl req -x509 -sha256 -nodes -days 3652 -newkey rsa:2048 ^
    -keyout .\%output_directory%\%root_cert%.key ^
    -out .\%output_directory%\%root_cert%.crt -subj "/C=TH/CN=localhost-root-ca"

type .\%output_directory%\%root_cert%.key .\%output_directory%\%root_cert%.crt > .\%output_directory%\%root_cert%.pem
    
REM Generate localhost certificate files.
echo Generate localhost certificate files.
(
    echo authorityKeyIdentifier=keyid,issuer
    echo basicConstraints=CA:FALSE
    echo keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    echo subjectAltName = @alt_names
    echo [alt_names]
    echo DNS.1 = localhost
) > .\%output_directory%\domains.ext

set localhost_cert=localhost-cert
openssl req -new -nodes -newkey rsa:2048 ^
    -keyout .\%output_directory%\%localhost_cert%.key ^
    -out .\%output_directory%\%localhost_cert%.csr -subj "/C=TH/ST=Bangkok/L=Localhost-CA/O=Localhost-Certificate-Authority/CN=localhost-ca"

openssl x509 -req -sha256 -days 3652 ^
    -in .\%output_directory%\%localhost_cert%.csr -CA .\%output_directory%\%root_cert%.crt -CAkey .\%output_directory%\%root_cert%.key -CAcreateserial -extfile .\%output_directory%\domains.ext ^
    -out .\%output_directory%\%localhost_cert%.crt

type .\%output_directory%\%localhost_cert%.key .\%output_directory%\%localhost_cert%.crt > .\%output_directory%\%localhost_cert%.pem

type .\%output_directory%\%localhost_cert%.crt .\%output_directory%\%root_cert%.crt > .\%output_directory%\%localhost_cert%.fullchain.crt

set pfx_pass=secret
openssl pkcs12 -export -inkey .\%output_directory%\%localhost_cert%.key ^
    -in .\%output_directory%\%localhost_cert%.fullchain.crt ^
    -out .\%output_directory%\%localhost_cert%.pfx -passout pass:"%pfx_pass%" -name "Localhost Certificate"

echo Done.
