#!/bin/bash

output_directory="output"
if [ -d "$output_directory" ]; then
    rm -rf $output_directory
fi

mkdir $output_directory

# Generate root certificat files.
echo "Generate root certificat files."
root_cert="root-cert"
openssl req -x509 -sha256 -nodes -days 3652 -newkey rsa:2048 \
  -keyout ./$output_directory/$root_cert.key \
  -out ./$output_directory/$root_cert.crt -subj "/C=TH/CN=localhost-root-ca"

cat ./$output_directory/$root_cert.key ./$output_directory/$root_cert.crt > ./$output_directory/$root_cert.pem

# Generate local certificat files.
echo "Generate local certificat files."
cat > ./$output_directory/domains.ext << EOL
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = localhost
EOL

localhost_cert="localhost-cert"
openssl req -new -nodes -newkey rsa:2048 \
  -keyout ./$output_directory/$localhost_cert.key \
  -out ./$output_directory/$localhost_cert.csr -subj "/C=TH/ST=Bangkok/L=Localhost-CA/O=Localhost-Certificate-Authority/CN=localhost-ca"

openssl x509 -req -sha256 -days 3652 \
  -in ./$output_directory/$localhost_cert.csr -CA ./$output_directory/$root_cert.crt -CAkey ./$output_directory/$root_cert.key -CAcreateserial -extfile ./$output_directory/domains.ext \
  -out ./$output_directory/$localhost_cert.crt

cat ./$output_directory/$localhost_cert.key ./$output_directory/$localhost_cert.crt > ./$output_directory/$localhost_cert.pem

cat ./$output_directory/$localhost_cert.crt ./$output_directory/$root_cert.crt > ./$output_directory/$localhost_cert.fullchain.crt

pfx_pass="secret"
openssl pkcs12 -export -inkey ./$output_directory/$localhost_cert.key \
  -in ./$output_directory/$localhost_cert.fullchain.crt \
  -out ./$output_directory/$localhost_cert.pfx -passout pass:"$pfx_pass" -name "Localhost Certificate"

echo "Done."
