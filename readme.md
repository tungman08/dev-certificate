## OpenSSL command for local certificate

### Root Certificate
#### 1. Generate root-cert.key & root-cert.crt
- Shell:
```
openssl req -x509 -sha256 -nodes -days 3652 -newkey rsa:2048 \
  -keyout root-cert.key \
  -out root-cert.crt -subj "/C=TH/CN=localhost-root-ca"
```
- PowerShell:
```
openssl req -x509 -sha256 -nodes -days 3652 -newkey rsa:2048 `
  -keyout root-cert.key `
  -out root-cert.crt -subj "/C=TH/CN=localhost-root-ca"
```
- CMD
```
openssl req -x509 -sha256 -nodes -days 3652 -newkey rsa:2048 ^
  -keyout root-cert.key ^
  -out root-cert.crt -subj "/C=TH/CN=localhost-root-ca"
```

#### 2. Create root-cert.pem
- Shell:
```
cat root-cert.key root-cert.crt > root-cert.pem
```
- PowerShell:
```
Get-Content root-cert.key, root-cert.crt | Set-Content root-cert.pem
```
- CMD
```
type root-cert.key root-cert.crt > root-cert.pem
```

---

### Domain Extension File
#### Generate domains.ext
- Shell:
```
cat > domains.ext << EOL
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = localhost
EOL
```
- PowerShell:
```
@"
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = localhost
"@ > domains.ext
```
- CMD
```
(
  echo authorityKeyIdentifier=keyid,issuer
  echo basicConstraints=CA:FALSE
  echo keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
  echo subjectAltName = @alt_names
  echo [alt_names]
  echo DNS.1 = localhost
) > domains.ext
```

---

### Localhost Certificate
#### 1. Generate localhost-cert.key & localhost-cert.csr
- Shell:
```
openssl req -new -nodes -newkey rsa:2048 \
  -keyout localhost-cert.key \
  -out localhost-cert.csr -subj "/C=TH/ST=Bangkok/L=Localhost-CA/O=Localhost-Certificate-Authority/CN=localhost-ca"
```
- PowerShell:
```
openssl req -new -nodes -newkey rsa:2048 `
  -keyout localhost-cert.key `
  -out localhost-cert.csr -subj "/C=TH/ST=Bangkok/L=Localhost-CA/O=Localhost-Certificates-Authority/CN=localhost-ca"
```
- CMD
```
openssl req -new -nodes -newkey rsa:2048 ^
  -keyout localhost-cert.key ^
  -out localhost-cert.csr -subj "/C=TH/ST=Bangkok/L=Localhost-CA/O=Localhost-Certificate-Authority/CN=localhost-ca"
```

#### 2. Generate localhost-cert.crt
- Shell:
```
openssl x509 -req -sha256 -days 3652 \
  -in localhost-cert.csr -CA root-cert.crt -CAkey root-cert.key -CAcreateserial -extfile domains.ext \
  -out localhost-cert.crt
```
- PowerShell:
```
openssl x509 -req -sha256 -days 3652 `
  -in localhost-cert.csr -CA root-cert.crt -CAkey root-cert.key -CAcreateserial -extfile domains.ext `
  -out localhost-cert.crt
```
- CMD
```
openssl x509 -req -sha256 -days 3652 ^
  -in localhost-cert.csr -CA root-cert.crt -CAkey root-cert.key -CAcreateserial -extfile domains.ext ^
  -out localhost-cert.crt
```

#### 3. Create localhost-cert.pem
- Shell:
```
cat localhost-cert.key localhost-cert.crt > localhost-cert.pem
```
- PowerShell:
```
Get-Content localhost-cert.key, localhost-cert.crt | Set-Content localhost-cert.pem
```
- CMD
```
type localhost-cert.key localhost-cert.crt > localhost-cert.pem
```

#### 4. Create localhost.fullchain.crt
- Shell:
```
cat localhost-cert.crt root-cert.crt > localhost-cert.fullchain.crt
```
- PowerShell:
```
Get-Content localhost-cert.crt, root-cert.crt | Set-Content localhost-cert.fullchain.crt
```
- CMD
```
type localhost-cert.crt root-cert.crt > localhost-cert.fullchain.crt
```

#### 5. Generate localhost-cert.pfx
- Shell:
```
openssl pkcs12 -export -inkey localhost-cert.key \
  -in localhost-cert.fullchain.crt \
  -out localhost-cert.pfx -name "Localhost Certificate"
```
- PowerShell:
```
openssl pkcs12 -export -inkey localhost-cert.key `
  -in localhost-cert.fullchain.crt `
  -out localhost-cert.pfx -name "Localhost Certificate"
```
- CMD
```
openssl pkcs12 -export -inkey localhost-cert.key ^
  -in localhost-cert.fullchain.crt ^
  -out localhost-cert.pfx -name "Localhost Certificate"
```

---

### Install certificates
#### Windows
> - Open the "Microsoft Management Console" by using the Windows + R keyboard combination, typing 'mmc' and clicking Open
> - Go to File > Add/Remove Snap-in
> - Click Certificates and Add
> - Select Computer Account and click Next
> - Select Local Computer then click Finish
> - Click OK to go back to the MMC window
> - Double-click Certificates (local computer) to expand the view
> - Select Trusted Root Certification Authorities, right-click on Certificates in the middle column under “Object Type” and select All Tasks then Import
> - Click Next then Browse. Change the certificate extension dropdown next to the filename field to All Files (*.*) and locate the "root-cert.crt" file, click Open, then Next
> - Select Place all certificates in the following store. "Trusted Root Certification Authorities store" is the default. Click Next then click Finish to complete the wizard.
> - Restart your computer.

#### MacOS
```
sudo security add-trusted-cert -d -r trustRoot -k "/Library/Keychains/System.keychain" root-cert.crt
```
- Restart your computer.

#### Linux
```
sudo apt-get install -y ca-certificates
sudo cp root-cert.crt /usr/local/share/ca-certificates/root-cert.crt
sudo update-ca-certificates
```
- Restart your computer.

---

### Use certificates
#### ASP.Net
- Add the "Kestrel" to your "appsettings.Development.json" file.
```
{
  ...
  "Kestrel": {
    "Certificates": {
      "Default": {
        "Path": "<your path>/localhost-cert.pfx",
        "Password": "<your password>"
      }
    }
  },
  ...
}
```
- Run the CLI tool to start your applocation in development mode.
```
dotnet run -lp https
```

#### Vite
- Add the https configuration to your "vite.config.ts" file.
```
import { defineConfig } from 'vite';
import fs from 'node:fs';

// https://vitejs.dev/config/
export default defineConfig({
  ...
  server: {
    https: {
      ca: fs.readFileSync('<your path>/root-cert.crt'),
      key:  fs.readFileSync('<your path>/localhost-cert.key'),
      cert: fs.readFileSync('<your path>/localhost-cert.crt')
    },
  },
  ...
});

```

#### Next.js
- Add the "experimental-https" options to "dev command" in your "package.json" file.
```
{
  ...
  "scripts": {
    "dev": "next dev --experimental-https --experimental-https-ca <your path>/root-cert.crt --experimental-https-key <your path>/localhost-cert.key --experimental-https-cert <your path>/localhost-cert.crt",
    ...
  },
  ...
}
```

#### Nuxt
- Coming soon.
