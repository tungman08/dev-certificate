# powershell script

$OutputDirectory = "output"
if (Test-Path $OutputDirectory) {
    Remove-Item -Recurse -Force $OutputDirectory
}

New-Item -ItemType Directory -Path $OutputDirectory | Out-Null

# Generate root certificate files.
Write-Host "Generate root certificate files."
$RootCert = "root-cert"
openssl req -x509 -sha256 -nodes -days 3652 -newkey rsa:2048 `
    -keyout ".\$OutputDirectory\$RootCert.key" `
    -out ".\$OutputDirectory\$RootCert.crt" -subj "/C=TH/CN=localhost-root-ca"

Get-Content ".\$OutputDirectory\$RootCert.key", ".\$OutputDirectory\$RootCert.crt" | Set-Content ".\$OutputDirectory\$RootCert.pem"

# Generate local certificate files.
Write-Host "Generate local certificate files."
@"
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = localhost
"@ > ".\$OutputDirectory\domains.ext"

$LocalhostCert = "localhost-cert"
openssl req -new -nodes -newkey rsa:2048 `
  -keyout ".\$OutputDirectory\$LocalhostCert.key" `
  -out ".\$OutputDirectory\$LocalhostCert.csr" -subj "/C=TH/ST=Bangkok/L=Localhost-CA/O=Localhost-Certificate-Authority/CN=localhost-ca"

openssl x509 -req -sha256 -days 3652 `
  -in ".\$OutputDirectory\$LocalhostCert.csr" -CA ".\$OutputDirectory\$RootCert.crt" -CAkey ".\$OutputDirectory\$RootCert.key" -CAcreateserial -extfile ".\$OutputDirectory\domains.ext" `
  -out ".\$OutputDirectory\$LocalhostCert.crt"

Get-Content ".\$OutputDirectory\$LocalhostCert.key", ".\$OutputDirectory\$LocalhostCert.crt" | Set-Content ".\$OutputDirectory\$LocalhostCert.pem"

Get-Content ".\$OutputDirectory\$LocalhostCert.crt", ".\$OutputDirectory\$RootCert.crt" | Set-Content ".\$OutputDirectory\$LocalhostCert.fullchain.crt"

$PfxPass = "secret"
openssl pkcs12 -export -inkey ".\$OutputDirectory\$LocalhostCert.key" `
  -in ".\$OutputDirectory\$LocalhostCert.fullchain.crt" `
  -out ".\$OutputDirectory\$LocalhostCert.pfx" -passout pass:"$PfxPass" -name "Localhost Certificate"

Write-Host "Done."
  